# miniproj4_hw308
This project containerized a simple Rust Actix web app 'Hello World'. It built docker image and can run the container locally.

Live demo: demo_video.webm in the root directory

Screenshots: Posted below step by step

## Getting started

##### 1. Install docker [Docker](https://docs.docker.com/desktop/install/mac-install/)

##### 2. Create Actix Web Application using Rust [cargo](https://doc.rust-lang.org/cargo/getting-started/first-steps.html)

![](images/main.png)


##### 3. Add corresponding dependencies and bin into Cargo.toml

![](images/cargo.png)


##### 4. Create Dockerfile

![](images/dockerfile.png)


##### 5. Build Docker image: sudo docker build . -t miniproj4

![](images/build_docker.png)


##### 6. Docker Image created

![](images/docker_image.png) 


##### 7. Docker Container for Rust Actix Web app created

![](images/docker_container.png)


##### 8. Run Container Locally

![](images/run_local.png)





