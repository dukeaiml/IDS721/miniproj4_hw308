# Use an official Rust image 
# It's best to match your own version of rust
FROM rust:1.75 as builder

# Create a new empty shell project
WORKDIR /usr/src/actix_web_app

COPY . .

RUN cargo build --release
# Expose port 8080
EXPOSE 8080
CMD cargo run


